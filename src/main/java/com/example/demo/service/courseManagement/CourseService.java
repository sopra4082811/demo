package com.example.demo.service.courseManagement;

import com.example.demo.entity.courseManagement.Course;
import com.example.demo.repository.courseManagement.CourseRepo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  This service class manages the application logic to manage Courses.
 */

@Service
public class CourseService {

  private final CourseRepo courseRepo;

  /**
   * Constructor for spring dependency injection.
   */
  public CourseService(CourseRepo courseRepo) {
    this.courseRepo = courseRepo;
  }
  /**
   * Persists a course in the database.
   * @param course the course to persist.
   * @return persisted course object.
   */
  public Course persistCourse(Course course) {
    return courseRepo.save(course);
  }

  /**
   * Returns all courses persisted in the database.
   * @return List of courses.
   */
  public List<Course> getAllCourses() {
    return courseRepo.findAll();
  }


}
