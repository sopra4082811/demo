package com.example.demo.entity.userManagement;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

import java.util.Set;

@Entity
@Table(name = "benutzer")
public class Benutzer {

  @Id
  @GeneratedValue
  @Column(name = "benutzer_id")
  private Integer id;

  @Column(name = "username")
  private String username;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "enabled")
  private boolean enabled;

  @Column(name = "nonLocked")
  private boolean nonLocked;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "benutzer_rolle", joinColumns = @JoinColumn(name = "benutzer_id"), inverseJoinColumns = @JoinColumn(name = "rolle_id"))
  private Set<Rolle> rollen;


  ///////////////////////////////////////////////
  // Getter & Setter
  ///////////////////////////////////////////////


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isNonLocked() {
    return nonLocked;
  }

  public void setNonLocked(boolean nonLocked) {
    this.nonLocked = nonLocked;
  }

  public Set<Rolle> getRoles() {
    return rollen;
  }

  public void setRoles(Set<Rolle> rolles) {
    this.rollen = rolles;
  }

}
